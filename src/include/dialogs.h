// generated by Fast Light User Interface Designer (fluid) version 1.0308

#ifndef dialogs_h
#define dialogs_h
#include <FL/Fl.H>
#include <math.h>
#include "config.h"
#include "gettext.h"
#include "flamp_dialog.h"
#include "status.h"
#include "flamp.h"
#include "fileselect.h"
#include "debug.h"
#include "icons.h"
#include "xml_io.h"
#include "file_io.h"
#include "amp.h"
#include "ztimer.h"
#include "time_table.h"
#include "hamcast_group.h"
#include <FL/Fl_Double_Window.H>
extern Fl_Double_Window *dialog_flamp;
#include <FL/Fl_Menu_Bar.H>
extern Fl_Menu_Bar *mnubar;
extern void cb_mnu_folders(Fl_Menu_*, void*);
extern void cb_mnu_load_relay(Fl_Menu_*, void*);
extern void cb_mnu_save_relay(Fl_Menu_*, void*);
extern void cb_mnuExit(Fl_Menu_*, void*);
extern void cb_mnu_scripts(Fl_Menu_*, void*);
extern void cb_mnuEventLog(Fl_Menu_*, void*);
extern void cb_mnuOnLineHelp(Fl_Menu_*, void*);
extern void cb_mnuCmdLineParams(Fl_Menu_*, void*);
extern void cb_mnuAbout(Fl_Menu_*, void*);
#include <FL/Fl_Tabs.H>
extern void cb_tab(Fl_Tabs*, void*);
extern Fl_Tabs *tabs;
#include <FL/Fl_Group.H>
extern Fl_Group *Rx_tab;
#include <FL/Fl_Output.H>
extern Fl_Output *txt_rx_filename;
extern Fl_Output *txt_rx_datetime;
#include <FL/Fl_Button.H>
extern void cb_btn_save_file(Fl_Button*, void*);
extern Fl_Button *btn_save_file;
extern Fl_Output *txt_rx_descrip;
extern void cb_btn_rx_remove(Fl_Button*, void*);
extern Fl_Button *btn_rx_remove;
extern Fl_Output *txt_rx_callinfo;
extern Fl_Output *txt_rx_filesize;
extern Fl_Output *txt_rx_numblocks;
extern Fl_Output *txt_rx_blocksize;
extern void cb_btn_transfer_file_txQ(Fl_Button*, void*);
extern Fl_Button *btn_rxq_to_txq;
extern void cb_btn_copy_missing(Fl_Button*, void*);
extern Fl_Button *btn_copy_missing;
extern Fl_Output *txt_rx_missing_blocks;
extern Fl_BlockMap *rx_progress;
extern FTextView *txt_rx_output;
extern void cb_btn_send_relay(Fl_Button*, void*);
extern Fl_Button *btn_send_relay;
extern void cb_btn_parse_relay_blocks(Fl_Button*, void*);
extern Fl_Button *btn_parse_relay_blocks;
extern void cb_txt_relay_selected_blocks(Fl_Input2*, void*);
extern Fl_Input2 *txt_relay_selected_blocks;
#include <FL/Fl_Browser.H>
extern void cb_rx_queue(Fl_Browser*, void*);
extern Fl_Browser *rx_queue;
extern void cb_cnt_repeat_relay_data(Fl_Counter*, void*);
extern Fl_Counter *cnt_repeat_relay_data;
extern void cb_cnt_repeat_relay_header(Fl_Counter*, void*);
extern Fl_Counter *cnt_repeat_relay_header;
extern Fl_Output *txt_transfer_relay_size_time;
extern Fl_Group *Tx_tab;
extern void cb_tx_send_to(Fl_Input2*, void*);
extern Fl_Input2 *txt_tx_send_to;
extern Fl_Output *txt_tx_filename;
extern void cb_tx_descrip(Fl_Input2*, void*);
extern Fl_Input2 *txt_tx_descrip;
extern void cb_cnt_blocksize(Fl_Counter*, void*);
extern Fl_Counter *cnt_blocksize;
extern void cb_cnt_repeat_nbr(Fl_Counter*, void*);
extern Fl_Counter *cnt_repeat_nbr;
extern void cb_repeat_header(Fl_Counter*, void*);
extern Fl_Counter *cnt_repeat_header;
extern Fl_Output *txt_tx_numblocks;
extern void cb_use_encoder(Fl_ComboBox*, void*);
extern Fl_ComboBox *encoders;
extern void cb_cbo_modes(Fl_ComboBox*, void*);
extern Fl_ComboBox *cbo_modes;
extern Fl_Output *txt_transfer_size_time;
#include <FL/Fl_Check_Button.H>
extern void cb_use_compression(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_use_compression;
extern void cb_enable_tx_unproto(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_enable_tx_unproto;
extern void cb_selected_blocks(Fl_Input2*, void*);
extern Fl_Input2 *txt_tx_selected_blocks;
extern void cb_btn_send_file(Fl_Button*, void*);
extern Fl_Button *btn_send_file;
extern void cb_btn_send_queue(Fl_Button*, void*);
extern Fl_Button *btn_send_queue;
extern void cb_btn_tx_remove_file(Fl_Button*, void*);
extern Fl_Button *btn_remove_file;
extern void cb_btn_open_file(Fl_Button*, void*);
extern Fl_Button *btn_open_file;
#include <FL/Fl_Input.H>
extern void cb_drop_file(Fl_Input*, void*);
extern Fl_Input *drop_file;
extern void cb_btn_parse_blocks(Fl_Button*, void*);
extern Fl_Button *btn_parse_blocks;
extern void cb_tx_queue(Fl_Browser*, void*);
extern Fl_Browser *tx_queue;
extern Fl_Group *Events_tab;
extern Fl_Tabs *event_tabs;
extern Fl_Group *Timed_Events_tab;
extern void cb_repeat_at_times(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_repeat_at_times;
extern void cb_repeat_every(Fl_ComboBox*, void*);
extern Fl_ComboBox *cbo_repeat_every;
extern void cb_repeat_times(Fl_Input2*, void*);
extern Fl_Input2 *txt_repeat_times;
extern Fl_Check_Button *btn_auto_load_queue;
extern void cb_load_from_tx_folder(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_load_from_tx_folder;
extern void cb_manual_load_que(Fl_Button*, void*);
extern Fl_Button *btn_manual_load_queue;
extern void cb_auto_load_queue_path(Fl_Input2*, void*);
extern Fl_Input2 *txt_auto_load_queue_path;
extern void cb_repeat_forever(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_repeat_forever;
extern Fl_Output *outTimeValue;
#include <FL/Fl_Light_Button.H>
extern void cb_do_events(Fl_Light_Button*, void*);
extern Fl_Light_Button *do_events;
extern Fl_Group *Hamcast_Events_tab;
extern void cb_hamcast_mode_cycle(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_hamcast_mode_cycle;
extern Fl_Output *txt_hamcast_select_total_time;
extern void cb_hamcast_mode_enable_1(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_hamcast_mode_enable_1;
extern Fl_Output *txt_hamcast_select_1_time;
extern void cb_hamcast_mode_selection_1(Fl_ComboBox*, void*);
extern Fl_ComboBox *cbo_hamcast_mode_selection_1;
extern void cb_hamcast_mode_enable_2(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_hamcast_mode_enable_2;
extern Fl_Output *txt_hamcast_select_2_time;
extern void cb_hamcast_mode_selection_2(Fl_ComboBox*, void*);
extern Fl_ComboBox *cbo_hamcast_mode_selection_2;
extern void cb_hamcast_mode_enable_3(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_hamcast_mode_enable_3;
extern Fl_Output *txt_hamcast_select_3_time;
extern void cb_hamcast_mode_selection_3(Fl_ComboBox*, void*);
extern Fl_ComboBox *cbo_hamcast_mode_selection_3;
extern void cb_hamcast_mode_enable_4(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_hamcast_mode_enable_4;
extern Fl_Output *txt_hamcast_select_4_time;
extern void cb_hamcast_mode_selection_4(Fl_ComboBox*, void*);
extern Fl_ComboBox *cbo_hamcast_mode_selection_4;
extern Fl_Group *Config_tab;
extern void cb_tx_mycall(Fl_Input2*, void*);
extern Fl_Input2 *txt_tx_mycall;
extern void cb_tx_myinfo(Fl_Input2*, void*);
extern Fl_Input2 *txt_tx_myinfo;
extern void cb_sync_mode_flamp_fldigi(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_sync_mode_flamp_fldigi;
extern void cb_sync_mode_fldigi_flamp(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_sync_mode_fldigi_flamp;
extern void cb_fldigi_xmt_mode_change(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_fldigi_xmt_mode_change;
extern void cb_enable_tx_on_report(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_enable_tx_on_report;
extern void cb_enable_delete_warning(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_enable_delete_warning;
extern void cb_clear_tosend_on_tx_blocks(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_clear_tosend_on_tx_blocks;
extern void cb_disable_header_modem_on_block_fills(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_disable_header_modem_on_block_fills;
extern void cb_enable_unproto_markers(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_enable_unproto_markers;
extern void cb_auto_rx_save_local_time(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_auto_rx_save_local_time;
extern void cb_enable_header_modem(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_enable_header_modem;
extern void cb_header_modes(Fl_ComboBox*, void*);
extern Fl_ComboBox *cbo_header_modes;
extern void cb_enable_txrx_interval(Fl_Check_Button*, void*);
extern Fl_Check_Button *btn_enable_txrx_interval;
extern void cb_tx_interval_mins(Fl_Counter*, void*);
extern Fl_Counter *cnt_tx_interval_mins;
extern void cb_rx_interval_secs(Fl_Counter*, void*);
extern Fl_Counter *cnt_rx_interval_secs;
extern Fl_Output *txt_tx_interval;
extern Fl_Check_Button *btn_relay_retain_data;
extern Fl_Check_Button *btn_auto_rx_save;
extern Fl_Group *box_not_connected;
extern Fl_Group *box_connected;
Fl_Double_Window* flamp_dialog();
extern Fl_Menu_Item menu_mnubar[];
#define mnu_files (menu_mnubar+0)
#define mnu_folders (menu_mnubar+1)
#define mnu_load_relay (menu_mnubar+2)
#define mnu_save_relay (menu_mnubar+3)
#define mnu_exit (menu_mnubar+4)
#define mnu_script (menu_mnubar+6)
#define mnu_scripts (menu_mnubar+7)
#define mnu_help (menu_mnubar+9)
#define mnu_debug_log (menu_mnubar+10)
#define mnu_on_line_help (menu_mnubar+11)
#define mnu_command_line (menu_mnubar+12)
#define mnu_about (menu_mnubar+13)
#endif
