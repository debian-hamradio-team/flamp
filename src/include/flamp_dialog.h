#ifndef FLAMP_DIALOG_H
#define FLAMP_DIALOG_H

#include <string>

#include <FL/Fl.H>
#include <FL/Fl_Pixmap.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Round_Button.H>
#include <FL/Fl_Spinner.H>
#include <FL/Fl_Counter.H>
#include <FL/Fl_Multiline_Output.H>
#include <FL/Fl_Simple_Counter.H>
#include <FL/Fl_Hold_Browser.H>
#include <FL/Fl_Progress.H>

#include "FTextView.h"
#include "flinput2.h"
#include "combo.h"
#include "calendar.h"
#include "Fl_BlockMap.h"

#define CNT_BLOCK_SIZE_STEP_RATE	16
#define CNT_BLOCK_SIZE_MINIMUM		16
#define CNT_BLOCK_SIZE_MAXIMUM		2048

#define XMT_LABEL "Xmit"
#define CANX_LABEL "Cancel"
#define RELAY_LABEL "Relay"

extern Fl_Double_Window *main_window;

extern bool valid_mode_check(std::string &md);

extern void update_cbo_modes(std::string &fldigi_modes);
extern void unproto_widgets(class cAmp *amp);
extern void set_txt_tx_interval();
extern bool assign_bc_modem_list(void);
extern void estimate_bc(void);

extern std::string g_header_modem;
extern std::string g_modem;
extern std::string selected_encoder_string;

enum tab_number {
	rx_tab_idx       = 0,
	tx_tab_idx       = 1,
	events_tab_idx   = 2,
	config_tab_idx   = 3,
	tab_count
};

extern Fl_Group *tab_pointers[tab_count];

#endif
