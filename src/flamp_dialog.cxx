// =====================================================================
//
// flamp_dialog.cxx
//
// Author(s):
//	Dave Freese, W1HKJ, Copyright (C) 2010, 2011, 2012, 2013
//  Robert Stiles, KK5VD, Copyright (C) 2013, 2014, 2015
//
// This file is part of FLAMP.
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// =====================================================================


#include "dialogs.h"
#include "flamp_dialog.h"
//======================================================================

Fl_Double_Window * main_window = 0;
Fl_Double_Window * wCmdLine    = 0;

extern void show_selected_xmt(int n);
extern void cb_load_tx_queue(void);
extern void cb_scripts_in_main_thread(void *);

char * s_modes[MAX_MODE_POINTERS];
char * s_modems = (char *) 0;
size_t no_of_modems = 0;

std::string valid_modes;

/** ********************************************************
 * A std::string table of event types
 ***********************************************************/
const char *event_types[] = {
	(char *) "5 min",
	(char *) "15 min",
	(char *) "30 min",
	(char *) "Hourly",
	(char *) "Even hours",
	(char *) "Odd hours",
	(char *) "Repeated at",
	(char *) "One time at",
	(char *) "Continuous at",
	(char *) 0
};

Fl_Group *tab_pointers[tab_count];

/** ********************************************************
 *
 ***********************************************************/
void redraw_rx_panel(void * data)
{
	if(Rx_tab) {
		txt_rx_filename->redraw();
		txt_rx_datetime->redraw();
		txt_rx_descrip->redraw();
		txt_rx_callinfo->redraw();
		txt_rx_filesize->redraw();
		txt_rx_numblocks->redraw();
		txt_rx_blocksize->redraw();
		txt_rx_output->redraw();
		txt_relay_selected_blocks->redraw();
		rx_progress->redraw();
		cnt_repeat_relay_data->redraw();
		cnt_repeat_relay_header->redraw();
		txt_transfer_relay_size_time->redraw();
	}
}

/** ********************************************************
 *
 ***********************************************************/
inline void add_a_file_message(void)
{
 	fl_alert("%s", "Add a file to the TX queue");
}

/** ********************************************************
 *
 ***********************************************************/
bool valid_mode_check(std::string &md)
{
	return (valid_modes.find(md) != std::string::npos);
}

/** ********************************************************
 *
 ***********************************************************/
size_t create_script_modem_check_list(void)
{
	size_t str_length = 0;
	size_t str_index = 0;

	if(s_modems)
		delete s_modems;

	std::memset(s_modes, 0, sizeof(s_modes));
	s_modems = (char *)0;
	no_of_modems = 0;
	str_length = valid_modes.size();

	s_modems = new char [str_length + 1];

	if(s_modems) {
		std::memcpy(s_modems, valid_modes.c_str(), sizeof(char) * str_length);
		s_modems[str_length] = 0;

		while(1) {
			while(str_index < str_length) {
				if(s_modems[str_index] > ' ') break;
				str_index++;
			}

			if(no_of_modems >= MAX_MODE_POINTERS || str_index >= str_length)
				break;

			if(s_modems[str_index] != '|')
				s_modes[no_of_modems++] = &s_modems[str_index];
			else break;

			while(str_index < str_length) {
				if(s_modems[str_index] == '|') {
					s_modems[str_index++] = 0;
					break;
				}
				str_index++;
			}
		}
	}

	return no_of_modems;
}

/** ********************************************************
 *
 ***********************************************************/
void update_cbo_modes(std::string &fldigi_modes)
{
	size_t count = fldigi_modes.size();
	size_t start = 0, end = 0;
	std::string modem_list, modem_name;
	char *cPtr = (char *)0;

	valid_modes.clear();
	cbo_modes->clear();
	cbo_header_modes->clear();
	cbo_hamcast_mode_selection_1->clear();
	cbo_hamcast_mode_selection_2->clear();
	cbo_hamcast_mode_selection_3->clear();
	cbo_hamcast_mode_selection_4->clear();

	modem_list.assign(fldigi_modes);
	modem_name.clear();

	if(count < 1) {
		LOG_INFO("update_cbo_modes(): Modem List contains no data!");
		return;
	}

	LOG_INFO("Modem List: %s", modem_list.c_str());

	while(start < count && end < count) {

		end = modem_list.find("|", start);

		if(end == std::string::npos) break;

		modem_name.assign(modem_list.substr(start, end - start));

		LOG_INFO("Modem: %s",  modem_name.c_str());

		start = end + 1;
		cPtr = (char *) modem_name.c_str();

		if(!cPtr) break;

		cbo_modes->add(cPtr);
		cbo_header_modes->add(cPtr);
		cbo_hamcast_mode_selection_1->add(cPtr);
		cbo_hamcast_mode_selection_2->add(cPtr);
		cbo_hamcast_mode_selection_3->add(cPtr);
		cbo_hamcast_mode_selection_4->add(cPtr);

		valid_modes.append(cPtr).append("|");
	}

	create_script_modem_check_list();

	cbo_modes->index(progStatus.selected_mode);
	cbo_header_modes->index(progStatus.header_selected_mode);
	cbo_hamcast_mode_selection_1->index(progStatus.hamcast_mode_selection_1);
	cbo_hamcast_mode_selection_2->index(progStatus.hamcast_mode_selection_2);
	cbo_hamcast_mode_selection_3->index(progStatus.hamcast_mode_selection_3);
	cbo_hamcast_mode_selection_4->index(progStatus.hamcast_mode_selection_4);
	assign_bc_modem_list();
	g_modem.assign(cbo_modes->value());
	g_header_modem.assign(cbo_header_modes->value());

}

/** ********************************************************
 * Initialize a minimal set of available modems
 ***********************************************************/
void init_cbo_modes()
{
	std::string min_modes;

	min_modes.assign( \
					"8PSK125|8PSK250|8PSK500|8PSK1000|8PSK125F|8PSK250F|" 	\
					"8PSK500F|8PSK1000F|8PSK1200F|BPSK31|BPSK63|BPSK63F|BPSK125|" 	\
					"BPSK250|BPSK500|BPSK1000|DOMX22|DOMX44|DOMX88|MFSK16|MFSK22|MFSK31|" 		\
					"MFSK32|MFSK64|MFSK64L|MFSK128|MFSK128L|MT63-500L|MT63-500S|MT63-1KL|" 		\
					"MT63-1KS|MT63-2KL|MT63-2KS|OLIVIA-4/250|OLIVIA-4/500|OLIVIA-8/250|" 		\
					"OLIVIA-8/500|OLIVIA-8/1K|OLIVIA-16/500|OLIVIA-16/1K|OLIVIA-32/1K|" 		\
					"OLIVIA-64/2K|PSK63RC4|PSK63RC5|PSK63RC10|PSK63RC20|PSK63RC32|PSK125R|" 	\
					"PSK125RC4|PSK125RC5|PSK125C12|PSK125RC10|PSK125RC12|PSK125RC16|PSK250R|" 	\
					"PSK250C6|PSK250RC2|PSK250RC3|PSK250RC5|PSK250RC6|PSK250RC7|PSK500R|PSK500C2|" \
					"PSK500C4|PSK500RC2|PSK500RC3|PSK500RC4|PSK800C2|PSK800RC2|PSK1000C2|" 		\
					"PSK1000R|PSK1000RC2|QPSK31|QPSK63|QPSK125|QPSK250|QPSK500|THOR16|THOR22|" 	\
					"THOR25x4|THOR50x1|THOR50x2|THOR100|THOR11|THOR32|THOR44|THOR56|" \
					 );

	update_cbo_modes(min_modes);
}

/** ********************************************************
 *
 ***********************************************************/
void init_cbo_events()
{
	int index = 0;

	while(event_types[index]) {
		cbo_repeat_every->add(event_types[index]);
		index++;
	}
	cbo_repeat_every->index(progStatus.repeat_every);
}

/** ********************************************************
 *
 ***********************************************************/
void cb_cbo_modes(Fl_ComboBox *a, void *b)
{
	progStatus.selected_mode = cbo_modes->index();
	g_modem.assign(cbo_modes->value());
	update_cAmp_changes(0);
	amp_mark_all_for_update();
	if (progStatus.sync_mode_flamp_fldigi)
		send_new_modem(cbo_modes->value());
	show_selected_xmt(0);
	estimate_relay();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_mnuExit(Fl_Menu_ *a, void *b)
{
	cb_exit();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_mnuEventLog(Fl_Menu_ *, void *)
{
	debug::show();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_mnuOnLineHelp(Fl_Menu_*, void*) {
	show_help();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_mnu_folders(Fl_Menu_*, void*) {
	cb_folders();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_mnu_scripts(Fl_Menu_*, void*) {
	cb_scripts(false);
}

/** ********************************************************
 *
 ***********************************************************/
void cb_mnuAbout(Fl_Menu_ *, void *)
{
	fl_message2("\tFlamp: %s\n\n" \
				"\tAuthors:\n" \
				"\t\tDave Freese, W1HKJ\n" \
				"\t\tRobert Stiles, KK5VD",
				FLAMP_VERSION);
}

/** ********************************************************
 *
 ***********************************************************/
void cb_mnu_load_relay(Fl_Menu_ *, void *)
{
	process_relay_files();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_mnu_save_relay(Fl_Menu_ *, void *)
{
	save_all_rx_relay_files();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_mnuCmdLineParams(Fl_Menu_ *, void *)
{
	if (!wCmdLine) {
		wCmdLine = new Fl_Double_Window(0,0,604,404,"Command Line Options");
		wCmdLine->begin();
		Fl_Browser *bwsCmds = new Fl_Browser(2,2,600,400,"");
		int i = 0;
		std::string cmdline;
		while (options[i] != NULL) {
			cmdline.assign("@f").append(options[i]);
			bwsCmds->add(cmdline.c_str());
			i++;
		}
		wCmdLine->end();
	}
	wCmdLine->show();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_tx_mycall(Fl_Input2*, void*)
{
	progStatus.my_call = txt_tx_mycall->value();
	update_cAmp_changes(0);
	show_selected_xmt(tx_queue->value());
}

/** ********************************************************
 *
 ***********************************************************/
void cb_tx_myinfo(Fl_Input2*, void*)
{
	progStatus.my_info = txt_tx_myinfo->value();
	update_cAmp_changes(0);
	show_selected_xmt(tx_queue->value());
}

/** ********************************************************
 *
 ***********************************************************/
void cb_tx_send_to(Fl_Input2*, void*)
{
	update_cAmp_changes(0);
	show_selected_xmt(tx_queue->value());
}

/** ********************************************************
 *
 ***********************************************************/
void cb_tx_descrip(Fl_Input2*, void*)
{
	update_cAmp_changes(0);
	show_selected_xmt(tx_queue->value());
}

/** ********************************************************
 *
 ***********************************************************/
void cb_selected_blocks(Fl_Input2*, void*)
{
	update_cAmp_changes(0);
	show_selected_xmt(tx_queue->value());
}

/** ********************************************************
 *
 ***********************************************************/
void cb_btn_save_file(Fl_Button*, void*)
{
	writefile(0);
}

/** ********************************************************
 *
 ***********************************************************/
void cb_btn_transfer_file_txQ(Fl_Button*, void*)
{
	writefile(1);
}

/** ********************************************************
 *
 ***********************************************************/
void cb_btn_rx_remove(Fl_Button*, void*)
{
	receive_remove_from_queue(false);
}

/** ********************************************************
 *
 ***********************************************************/
void cb_btn_open_file(Fl_Button*, void*)
{
	readfile();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_btn_tx_remove_file(Fl_Button*, void*)
{
	tx_removefile(false);
}

/** ********************************************************
 *
 ***********************************************************/
void cb_btn_copy_missing(Fl_Button*, void*)
{
	send_missing_report();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_btn_parse_blocks(Fl_Button*, void*)
{
	recv_missing_report();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_tx_queue(Fl_Browser *hb, void*)
{
	int value = tx_queue->value();
	show_selected_xmt(value);
}

/** ********************************************************
 *
 ***********************************************************/
void cb_rx_queue(Fl_Browser *hb, void*)
{
	int n = hb->value();
	show_selected_rcv(n);
}

/** ********************************************************
 *
 ***********************************************************/
void cb_btn_send_file(Fl_Button*, void*)
{
	if(transmitting) {
		if (do_events_flag == 1) {
			do_events_flag = 0;
			do_events->value(0);
			stop_events();
			do_events->label("Start Events");
			do_events->redraw_label();
		} else {
			abort_request();
		}
		return;
	}

	if ((tx_queue->value() == 0) || (tx_queue->size() == 0)) 
	{
		add_a_file_message();
		return;
	}

	btn_send_queue->deactivate();

	transmit_current();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_btn_send_queue(Fl_Button*, void*)
{
	if(transmitting) {
		abort_request();
		return;
	}

	if (tx_queue->size() == 0) 	{
		add_a_file_message();
		return;
	}

	btn_send_queue->deactivate();

	if(Fl::event_shift())
		transmit_queued(false, true);
	else
		transmit_queued(false, false);

}

/** ********************************************************
 *
 ***********************************************************/
void cb_cnt_blocksize(Fl_Counter*, void*)
{
	int request_blk_size = (int)cnt_blocksize->value();
	int reset_flag = false;

	if(progStatus.blocksize != request_blk_size)
		reset_flag = true;

	progStatus.blocksize = request_blk_size;
	update_cAmp_changes(0);
	amp_mark_all_for_update();
	show_selected_xmt(tx_queue->value());

	if(reset_flag)
		txt_tx_selected_blocks->value("");
}

/** ********************************************************
 *
 ***********************************************************/
void cb_cnt_repeat_nbr(Fl_Counter*, void*)
{
	progStatus.repeatNN = (int)cnt_repeat_nbr->value();
	update_cAmp_changes(0);
	amp_mark_all_for_update();
	show_selected_xmt(tx_queue->value());
}

/** ********************************************************
 *
 ***********************************************************/
void cb_repeat_header(Fl_Counter*, void*)
{
	progStatus.repeat_header = (int)cnt_repeat_header->value();
	update_cAmp_changes(0);
	amp_mark_all_for_update();
	show_selected_xmt(tx_queue->value());
}

/** ********************************************************
 *
 ***********************************************************/
void cb_use_compression(Fl_Check_Button*, void*)
{
	bool req_compression = btn_use_compression->value();
	int reset_flag = false;

	if(progStatus.use_compression != req_compression)
		reset_flag = true;

	progStatus.use_compression = req_compression;
	update_cAmp_changes(0);
	show_selected_xmt(tx_queue->value());

	if(reset_flag)
		txt_tx_selected_blocks->value("");
}

/** ********************************************************
 *
 ***********************************************************/
void cb_use_encoder(Fl_ComboBox *w, void *)
{
	int req_encoder = encoders->index()+1;
	int reset_flag = false;

	if(progStatus.encoder != req_encoder)
		reset_flag = true;

	progStatus.encoder = req_encoder;
	progStatus.encoder_string.assign(encoders->value());
	update_cAmp_changes(0);
	show_selected_xmt(tx_queue->value());

	if(reset_flag)
		txt_tx_selected_blocks->value("");
}

/** ********************************************************
 *
 ***********************************************************/
void init_encoders()
{
	encoders->clear();
	encoders->add("base64");
	encoders->add("base128");
	encoders->add("base256");
	encoders->index(progStatus.encoder-1);
	progStatus.encoder_string.assign(encoders->value());
}

/** ********************************************************
 *
 ***********************************************************/
void cb_sync_mode_flamp_fldigi(Fl_Check_Button *b, void *)
{
	progStatus.sync_mode_flamp_fldigi = btn_sync_mode_flamp_fldigi->value();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_sync_mode_fldigi_flamp(Fl_Check_Button *b, void *)
{
	progStatus.sync_mode_fldigi_flamp = btn_sync_mode_fldigi_flamp->value();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_fldigi_xmt_mode_change(Fl_Check_Button *b, void *)
{
	if(progStatus.use_txrx_interval == true) {
		progStatus.fldigi_xmt_mode_change = true;
		btn_fldigi_xmt_mode_change->value(progStatus.fldigi_xmt_mode_change);
	} else {
		progStatus.fldigi_xmt_mode_change = btn_fldigi_xmt_mode_change->value();
	}
}

/** ********************************************************
 *
 ***********************************************************/
void cb_repeat_at_times(Fl_Check_Button *b, void *)
{
	progStatus.repeat_at_times = btn_repeat_at_times->value();
	if (progStatus.repeat_at_times) {
		btn_repeat_forever->value(0);
		progStatus.repeat_forever = false;
	}
}

/** ********************************************************
 *
 ***********************************************************/
void cb_auto_load_que(Fl_Check_Button *b, void *)
{
	int val = false;

	val = btn_auto_load_queue->value();
	progStatus.auto_load_queue_path.assign(txt_auto_load_queue_path->value());

	if(progStatus.auto_load_queue_path.size() < 1 && val == true) {
		progStatus.auto_load_queue = false;
		btn_auto_load_queue->value(false);
		return;
	}
	progStatus.auto_load_queue = val;
	btn_auto_load_queue->value(val);
}

/** ********************************************************
 *
 ***********************************************************/
void cb_load_from_tx_folder(Fl_Check_Button *b, void *)
{
	progStatus.load_from_tx_folder = btn_load_from_tx_folder->value();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_manual_load_que(Fl_Button *b, void *)
{
	cb_load_tx_queue();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_auto_load_queue_path(Fl_Input2 *b, void *)
{
	progStatus.auto_load_queue_path.assign(txt_auto_load_queue_path->value());
}

/** ********************************************************
 *
 ***********************************************************/
void cb_repeat_every(Fl_ComboBox *cb, void *)
{
	progStatus.repeat_every = cbo_repeat_every->index();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_repeat_times(Fl_Input2 *txt, void *)
{
	progStatus.repeat_times = txt_repeat_times->value();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_repeat_forever(Fl_Check_Button *b, void *)
{
	progStatus.repeat_forever = btn_repeat_forever->value();
	if (progStatus.repeat_forever) {
		btn_repeat_at_times->value(0);
		progStatus.repeat_at_times = false;
	}
}

/** ********************************************************
 *
 ***********************************************************/
void cb_drop_file(Fl_Input*, void*) {
	drop_file_changed();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_do_events(Fl_Light_Button *b, void*)
{
	if (tx_queue->size() == 0) {
		do_events->value(0);
		if(!generate_time_table) 
			add_a_file_message();
		return;
	}

	do_events_flag = do_events->value();

	if (do_events_flag) {
		do_events->label("Stop Events");
	} else {
		stop_events();
		do_events->label("Start Events");
	}
	do_events->redraw_label();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_enable_txrx_interval(Fl_Check_Button *a, void *b)
{
	progStatus.use_txrx_interval = (bool) btn_enable_txrx_interval->value();

	if(progStatus.use_txrx_interval == true) {
		progStatus.fldigi_xmt_mode_change = true;
		btn_fldigi_xmt_mode_change->value(progStatus.fldigi_xmt_mode_change);
	}
	show_selected_xmt(tx_queue->value());
}

/** ********************************************************
 *
 ***********************************************************/
#include <iostream>
void set_txt_tx_interval()
{
	char szT[25];
	int mins = floor(cnt_tx_interval_mins->value());
	int secs = round(60 * (cnt_tx_interval_mins->value() - mins));
	snprintf(szT, sizeof(szT), "%02d:%02d", mins, secs);
	txt_tx_interval->value(szT);
}

/** ********************************************************
 *
 ***********************************************************/
void cb_tx_interval_mins(Fl_Counter *a, void *b)
{
	progStatus.tx_interval_minutes = cnt_tx_interval_mins->value();
	set_txt_tx_interval();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_rx_interval_secs(Fl_Counter *a, void *b)
{
	progStatus.rx_interval_seconds = cnt_rx_interval_secs->value();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_enable_header_modem(Fl_Check_Button *a, void *b)
{
	progStatus.use_header_modem = btn_enable_header_modem->value();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_header_modes(Fl_ComboBox *a, void *b)
{
	progStatus.header_selected_mode = cbo_header_modes->index();
	g_header_modem.assign(cbo_header_modes->value());
}

/** ********************************************************
 *
 ***********************************************************/
void cb_disable_header_modem_on_block_fills(Fl_Check_Button *a, void *b)
{
	progStatus.disable_header_modem_on_block_fills = (bool) btn_disable_header_modem_on_block_fills->value();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_enable_tx_on_report(Fl_Check_Button *a, void *b)
{
	progStatus.use_tx_on_report = btn_enable_tx_on_report->value();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_clear_tosend_on_tx_blocks(Fl_Check_Button *a, void *b)
{
	progStatus.clear_tosend_on_tx_blocks = btn_clear_tosend_on_tx_blocks->value();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_enable_unproto_markers(Fl_Check_Button *a, void *b)
{
	progStatus.enable_unproto_markers = btn_enable_unproto_markers->value();
	update_cAmp_changes(0);
	show_selected_xmt(tx_queue->value());
}

/** ********************************************************
 *
 ***********************************************************/
void cb_enable_tx_unproto(Fl_Check_Button *a, void *b)
{
	progStatus.enable_tx_unproto = btn_enable_tx_unproto->value();

	if(progStatus.enable_tx_unproto) {
		btn_use_compression->value(0);
		progStatus.use_compression = 0;
	}

	update_cAmp_changes(0);
	show_selected_xmt(tx_queue->value());
}

/** ********************************************************
 *
 ***********************************************************/
void unproto_widgets(cAmp *amp)
{
	if(amp)
		progStatus.enable_tx_unproto = amp->unproto();

	if(progStatus.enable_tx_unproto) {
		if(!progStatus.use_txrx_interval)
			cnt_blocksize->deactivate();
		cnt_repeat_header->deactivate();
		btn_use_compression->deactivate();
		encoders->deactivate();
		txt_tx_numblocks->deactivate();
	} else {
		cnt_blocksize->activate();
		cnt_repeat_header->activate();
		btn_use_compression->activate();
		encoders->activate();
		txt_tx_numblocks->activate();
		cnt_repeat_nbr->activate();
	}
}

/** ********************************************************
 *
 ***********************************************************/
void cb_hamcast_mode_cycle(Fl_Check_Button *a, void *b)
{
	progStatus.hamcast_mode_cycle = btn_hamcast_mode_cycle->value();
	if(progStatus.hamcast_mode_cycle) {
		progStatus.hamcast_mode_cycle = assign_bc_modem_list();
		btn_hamcast_mode_cycle->value(progStatus.hamcast_mode_cycle);
	}
	estimate_bc();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_hamcast_mode_enable_1(Fl_Check_Button *a, void *b)
{
	progStatus.hamcast_mode_enable_1 = btn_hamcast_mode_enable_1->value();
	if(!assign_bc_modem_list()) {
		progStatus.hamcast_mode_cycle = false;
		btn_hamcast_mode_cycle->value(progStatus.hamcast_mode_cycle);
	}
	estimate_bc();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_hamcast_mode_selection_1(Fl_ComboBox *a, void *b)
{
	progStatus.hamcast_mode_selection_1 = cbo_hamcast_mode_selection_1->index();
	if(!assign_bc_modem_list()) {
		progStatus.hamcast_mode_cycle = false;
		btn_hamcast_mode_cycle->value(progStatus.hamcast_mode_cycle);
	}
	estimate_bc();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_hamcast_mode_enable_2(Fl_Check_Button *a, void *b)
{
	progStatus.hamcast_mode_enable_2 = btn_hamcast_mode_enable_2->value();
	if(!assign_bc_modem_list()) {
		progStatus.hamcast_mode_cycle = false;
		btn_hamcast_mode_cycle->value(progStatus.hamcast_mode_cycle);
	}
	estimate_bc();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_hamcast_mode_selection_2(Fl_ComboBox*a, void *b)
{
	progStatus.hamcast_mode_selection_2 = cbo_hamcast_mode_selection_2->index();
	if(!assign_bc_modem_list()) {
		progStatus.hamcast_mode_cycle = false;
		btn_hamcast_mode_cycle->value(progStatus.hamcast_mode_cycle);
	}
	estimate_bc();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_hamcast_mode_enable_3(Fl_Check_Button *a, void *b)
{
	progStatus.hamcast_mode_enable_3 = btn_hamcast_mode_enable_3->value();
	if(!assign_bc_modem_list()) {
		progStatus.hamcast_mode_cycle = false;
		btn_hamcast_mode_cycle->value(progStatus.hamcast_mode_cycle);
	}
	estimate_bc();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_hamcast_mode_selection_3(Fl_ComboBox *a, void *b)
{
	progStatus.hamcast_mode_selection_3 = cbo_hamcast_mode_selection_3->index();
	if(!assign_bc_modem_list()) {
		progStatus.hamcast_mode_cycle = false;
		btn_hamcast_mode_cycle->value(progStatus.hamcast_mode_cycle);
	}
	estimate_bc();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_hamcast_mode_enable_4(Fl_Check_Button *a, void *b)
{
	progStatus.hamcast_mode_enable_4 = btn_hamcast_mode_enable_4->value();
	if(!assign_bc_modem_list()) {
		progStatus.hamcast_mode_cycle = false;
		btn_hamcast_mode_cycle->value(progStatus.hamcast_mode_cycle);
	}
	estimate_bc();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_hamcast_mode_selection_4(Fl_ComboBox *a, void *b)
{
	progStatus.hamcast_mode_selection_4 = cbo_hamcast_mode_selection_4->index();
	if(!assign_bc_modem_list()) {
		progStatus.hamcast_mode_cycle = false;
		btn_hamcast_mode_cycle->value(progStatus.hamcast_mode_cycle);
	}
	estimate_bc();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_enable_delete_warning(Fl_Check_Button *a, void *b)
{
	progStatus.enable_delete_warning = btn_enable_delete_warning->value();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_btn_send_relay(Fl_Button *a, void *b)
{
	if(transmitting) {
		abort_request();
		return;
	}
	send_relay_data();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_btn_parse_relay_blocks(Fl_Button *a, void *b)
{
	relay_missing_report();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_relay_selected_blocks(Fl_Button *a, void *b)
{
	update_rx_missing_blocks();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_auto_rx_save(Fl_Check_Button *a, void *b)
{
	progStatus.auto_rx_save = btn_auto_rx_save->value();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_relay_retain_data(Fl_Check_Button *a, void *b)
{
	progStatus.relay_retain_data = btn_relay_retain_data->value();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_auto_rx_save_local_time(Fl_Check_Button *a, void *b)
{
	progStatus.auto_rx_save_local_time = btn_auto_rx_save_local_time->value();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_tab(Fl_Tabs *a, void *b)
{
	int index = 0;
	Fl_Group *tmp =  (Fl_Group *) a->value();

	for(index = 0; index < tab_count; index++) {
		if(tmp == tab_pointers[index]) {
			break;
		}
	}
	
	progStatus.tab_saved_state = index;
}

/** ********************************************************
 *
 ***********************************************************/
void cb_cnt_repeat_relay_data(Fl_Counter *a, void *b)
{
	progStatus.repeat_relay_data = cnt_repeat_relay_data->value();
	estimate_relay();
}

/** ********************************************************
 *
 ***********************************************************/
void cb_cnt_repeat_relay_header(Fl_Counter *a, void *b)
{
	progStatus.repeat_relay_header = cnt_repeat_relay_header->value();
	estimate_relay();
}
/** ********************************************************
 *
 ***********************************************************/
void cb_txt_relay_selected_blocks(Fl_Input2 *a, void *b)
{
	estimate_relay();
}
